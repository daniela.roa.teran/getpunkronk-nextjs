import Link from "next/link";
import { useRouter } from "next/router";
import { useState, useContext } from "react";

interface NavigationProps {
  fixture: JSON;
}
const Navigation: React.FC<NavigationProps> = ({ fixture }) => {
  const [iconMenu, setIconMenu] = useState(false);
  //const { setCart } = useContext(CartContext);
  const router = useRouter();
  return (
    <>
      <div className="navigation--container">
        <nav>
          <div className="logo--container">
            <div className="left--logo">
              <img src="/images/logo.png" className="logo" alt="Logo"></img>
            </div>
            <div className="right--logo">
              <img src="/images/logo.png" className="logo" alt="Logo"></img>
            </div>
          </div>
          <div className="navbar--container__list">
            <ul>
              {fixture.options.map((option, i) => {
                return (
                  <li
                    key={`option-nav-${i}`}
                    className={
                      router.pathname === option.pathname ? "active" : ""
                    }
                  >
                    <Link
                      href={option.pathname}
                      onClick={() => setIconMenu(false)}
                    >
                      <a>{option.name}</a>
                    </Link>
                  </li>
                );
              })}
              <li>
                <button
                  className="cart--button"
                  //   onClick={() => setCart({ type: "open" })}
                >
                  <span className="mdi mdi-cart"></span>
                </button>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <style jsx>
        {`
          .navigation--container {
            width: 100vw;
            color: #fafafa;
            display: flex;
            flex-direction: row;
            flex-grow: 1;
            top: 0;
            position: fixed;
            filter: drop-shadow(2px 4px 6px black);
            z-index: 2;
          }
          .navigation--container .mobile--icon {
            z-index: 2;
            flex-grow: 1;
            justify-content: flex-end;
            font-size: 30px;
          }
          .navigation--container nav {
            flex-direction: row;
            flex-grow: 1;
            display: flex;
            padding: 0 15px;
            align-items: center;
            justify-content: space-around;
            background: rgba(0, 0, 0, 0.5);
          }
          .navigation--container nav .logo--container {
            display: flex;
            width: 210px;
            height: 67px;
            align-items: center;
            position: relative;
            margin-left: -70px;
          }
          .navigation--container nav .logo--container .right--logo,
          .navigation--container nav .logo--container .left--logo {
            transform: translateY(0px) rotate3d(1, 1, 10, 0deg);
            transition: transform 1s ease;
          }
          .navigation--container nav .logo--container:hover {
            cursor: pointer;
          }
          .navigation--container nav .logo--container:hover .right--logo {
            transform: translateY(20px) rotate3d(1, 1, 10, 17deg);
            transition: transform 1s ease;
          }
          .navigation--container nav .logo--container:hover .left--logo {
            transform: translateY(20px) rotate3d(1, 1, 10, -17deg);
            transition: transform 1s ease;
          }
          .navigation--container nav .logo--container .left--logo,
          .navigation--container nav .logo--container .right--logo {
            object-fit: contain;
            display: flex;
            overflow: hidden;
          }
          .navigation--container nav .logo--container .left--logo {
            height: 60px;
            width: 120px;
          }
          .navigation--container nav .logo--container .left--logo img {
            width: 100%;
            object-position: 74px;
          }
          .navigation--container nav .logo--container .right--logo {
            width: 120px;
            height: 60px;
          }
          .navigation--container nav .logo--container .right--logo img {
            object-position: -31px;
            width: 100%;
          }
          .navigation--container .navbar--container__list {
            display: flex;
          }
          .navigation--container .navbar--container__list ul {
            padding-left: 0;
            flex-direction: row;
            display: flex;
            flex-grow: 1;
            justify-content: flex-start;
            margin: 0;
          }
          .navigation--container .navbar--container__list ul * {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all 0.35s ease;
            transition: all 0.35s ease;
          }
          .navigation--container .navbar--container__list ul li {
            display: flex;
            justify-content: center;
            align-items: baseline;
            flex-grow: 0.1;
            text-align: center;
            list-style: outside none none;
            font-family: nunito;
            padding: 0;
            position: relative;
          }
          .navigation--container .navbar--container__list ul li a,
          .navigation--container .navbar--container__list ul li button {
            font-size: 16px;
            color: #fafafa;
            text-transform: uppercase;
            padding: 0 15px;
            height: 35px;
            line-height: 35px;
            text-decoration: none;
            position: relative;
            display: inline-flex;
            margin: 15px 10px;
            outline: none;
          }
          .navigation--container .navbar--container__list ul li a:before,
          .navigation--container .navbar--container__list ul li button:before,
          .navigation--container .navbar--container__list ul li a:after,
          .navigation--container .navbar--container__list ul li button:after {
            position: absolute;
            width: 35px;
            height: 2px;
            background: #fafafa;
            content: "";
            opacity: 0.2;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
            pointer-events: none;
          }
          .navigation--container .navbar--container__list ul li a:before,
          .navigation--container .navbar--container__list ul li button:before {
            top: 0;
            left: 0;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            transform: rotate(90deg);
            transform-origin: 0 0;
          }
          .navigation--container .navbar--container__list ul li a:after,
          .navigation--container .navbar--container__list ul li button:after {
            right: 0;
            bottom: 0;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            transform: rotate(90deg);
            transform-origin: 100% 2px;
          }
          .navigation--container .navbar--container__list ul li a:hover:before,
          .navigation--container
            .navbar--container__list
            ul
            li
            button:hover:before,
          .navigation--container .navbar--container__list ul li.active a:before,
          .navigation--container
            .navbar--container__list
            ul
            li
            button.current:before {
            left: 50%;
            transform: rotate(0deg) translateX(-50%);
            opacity: 1;
            background: #00fa00;
          }
          .navigation--container .navbar--container__list ul li a:hover:after,
          .navigation--container
            .navbar--container__list
            ul
            li
            button:hover:after,
          .navigation--container .navbar--container__list ul li.active a:after,
          .navigation--container
            .navbar--container__list
            ul
            li
            button.current:after {
            right: 50%;
            transform: rotate(0deg) translateX(50%);
            opacity: 1;
            background: #00fa00;
          }
          @media screen and (max-width: 768px) {
            .navigation--container .navbar--container__list {
              position: absolute;
              background: #fafafa;
              top: 0;
              left: 0;
              overflow: hidden;
              height: 100vh;
              transition: width 0.5s ease;
            }
            .navigation--container .navbar--container__list ul {
              width: 100%;
              flex-direction: column;
              padding: 20px;
            }
            .navigation--container .navbar--container__list ul li {
              padding: 10px 0;
            }
            .navigation--container .navbar--container__list ul li a,
            .navigation--container .navbar--container__list ul li button {
              color: #212121;
            }
          }
        `}
      </style>
    </>
  );
};
export default Navigation;
