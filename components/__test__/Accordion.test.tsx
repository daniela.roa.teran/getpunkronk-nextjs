import React from "react";
import { shallow } from "enzyme";
import Accordion from "../Accordion";
describe("MyComponent", () => {
  it('should render correctly in "debug" mode', () => {
    const component = shallow(
      <Accordion title="Description">esta es una descripcion</Accordion>
    );
    expect(component).toMatchSnapshot();
  });
});
