import { ThemeContext } from "../../contexts/ThemeContext";
import { useContext, useState } from "react";
import appTheme from "../../styles/appTheme";

interface AppThemeProps {
  theme: string;
  children: React.ReactNode;
}

const AppLayoutTheme: React.FC<AppThemeProps> = (props) => {
  const { theme, setTheme } = useContext(ThemeContext);
  const [currentTheme, changeCurrentTheme] = useState(theme.type);
  const styles = currentTheme === "light" ? appTheme.light : appTheme.dark;
  return (
    <>
      <button
        onClick={() => {
          changeCurrentTheme(currentTheme === "light" ? "dark" : "light");
          setTheme({ type: currentTheme });
        }}
      >
        Change theme
      </button>
      {props.children}
      <style jsx global>{`
        body {
          background-color: ${styles.body};
          color: ${styles.text};
        }
      `}</style>
    </>
  );
};
export default AppLayoutTheme;
