import React from "react";
import { useState } from "react";

interface AccordionProps {
  title: string;
  children: React.ReactNode;
}
const Accordion: React.FC<AccordionProps> = ({ title, children }) => {
  const [state, setState] = useState<Boolean>(false);
  return (
    <>
      <div
        className={`accordion--element ${state ? "open" : "closed"}`}
        onClick={() => {
          setState(!open);
        }}
      >
        <div className="title--container">
          <h4>{title}</h4>
          <span
            className={`mdi ${state ? "mdi-chevron-up" : "mdi-chevron-down"} `}
          ></span>
        </div>
        <p>{children}</p>
      </div>
      <style jsx>{`
        .accordion--element {
          border-bottom: 2px solid #00fa00;
          cursor: pointer;
        }
        .accordion--element h4 {
          margin-bottom: 10px;
        }
        .accordion--element.closed p {
          margin-bottom: 0px;
          max-height: 0;
          transition: max-height 0.35s cubic-bezier(0, 1, 0, 1),
            margin-bottom 0.3s ease;
        }
        .accordion--element p {
          margin: 0;
          overflow: hidden;
          transition: max-height 0.3s cubic-bezier(1, 0, 1, 0),
            margin-bottom 0.3s ease;
          height: auto;
          max-height: 9999px;
          margin-bottom: 15px;
        }
        .title--container {
          display: flex;
          justify-content: space-between;
          align-items: center;
        }
        .title--container .mdi {
          font-size: 22px;
        }
      `}</style>
    </>
  );
};
export default Accordion;
