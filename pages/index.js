import Head from "next/head";
import styles from "../styles/Home.module.css";
import { ThemeProvider } from "../contexts/ThemeContext";
import Navigation from "../components/Navigation";
import AppLayoutTheme from "../components/AppLayoutTheme";
import navigation from "../fixtures/navigation.json";

export default function Home() {
  return (
    <ThemeProvider>
      <div className={styles.container}>
        <Head>
          <title>Create Next App</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <AppLayoutTheme>
          <Navigation fixture={navigation.ES} />
          <main className={styles.main}>
            <h1 className={styles.title}>
              Welcome to <a href="https://nextjs.org">Next.js!</a>
            </h1>
            <p>ksdnlkadnslkansdlnadsnaldnslkansdnkaslkdnalksndlkanslkn</p>
          </main>
        </AppLayoutTheme>

        <footer className={styles.footer}></footer>
        <style jsx>
          {`
            h1 {
              background-color: red;
            }
          `}
        </style>
      </div>
    </ThemeProvider>
  );
}
