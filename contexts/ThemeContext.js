import { useMemo, createContext, useReducer } from "react";
import appTheme from "../styles/appTheme";

const initialState = { type: "light", layout: appTheme.light };
function init(initialState) {
  return initialState;
}
//definition and exportation of theme context
const ThemeContext = createContext(initialState);
let reducer = (state, action) => {
  switch (action.type) {
    case "light":
      return { ...state, type: "dark", layout: appTheme.dark };
    case "dark":
      return { ...state, type: "light", layout: appTheme.light };
    default:
      return initialState;
  }
};
//definition and exportation of theme provider
const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useReducer(reducer, initialState);
  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      {children}
    </ThemeContext.Provider>
  );
};
export { ThemeContext, ThemeProvider };
