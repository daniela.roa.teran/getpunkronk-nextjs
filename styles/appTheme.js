const lightTheme = {
  body: "#fafafa",
  text: "#212121",
};

const darkTheme = {
  body: "#212121",
  text: "#fafafa",
};

export default {
  light: lightTheme,
  dark: darkTheme,
};
